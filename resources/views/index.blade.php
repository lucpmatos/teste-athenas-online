@extends('template')

@section('title')
    Teste Athenas Online
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <p>O desafio é criar um crud simples em PHP e que salve os dados no banco de dados (Firebird, MySQL, Mongo, PostgreSQL etc).</p>
            <p>Para avaliar o teste basta navegar pelo menu de navegação no topo da página.</p>
        </div>
    </div>
@endsection
