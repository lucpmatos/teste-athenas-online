@extends('template')

@section('title')
    Usuários
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{ route('users.index') }}" class="btn btn-secondary">< Voltar</a>
        </div>
        <div class="col-12">
            <hr/>
        </div>
    </div>
    <form action="{{ route('users.store') }}" method="post" class="row">
        @csrf

        <div class="col-12">
            <div class="form-group">
                <label for="name">Nome completo:</label>
                <input type="text" name="name" id="name" placeholder="Nome" class="form-control" required>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="email">E-mail:</label>
                <input type="text" name="email" id="email" placeholder="E-mail" class="form-control" required>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="category_id">Categoria:</label>
                <select name="category_id" id="category_id" class="form-control" required>
                    <option value="">Selecione uma categoria...</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-12">
            <button type="submit" class="btn btn-success float-right">Salvar</button>
        </div>

    </form>
@endsection
