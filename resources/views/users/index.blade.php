@extends('template')

@section('title')
    Usuários
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            {{--<a href="{{ route('users.create') }}" class="btn btn-primary">Novo</a>--}}
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#novoUsuario">
                Novo
            </button>
            <!-- Modal -->
            <div class="modal fade" id="novoUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <form id="formNewUser" action="" class="modal-content">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Novo usuário</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Nome completo:</label>
                                        <input type="text" name="name" id="name" placeholder="Nome" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="email">E-mail:</label>
                                        <input type="text" name="email" id="email" placeholder="E-mail" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="category_id">Categoria:</label>
                                        <select name="category_id" id="category_id" class="form-control" required>
                                            <option value="">Selecione uma categoria...</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <hr/>
        </div>
        <div class="col-12">
            @include('partials.message')
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Categoria</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->category->name }}</td>
                        <td>
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-sm float-left mr-1">Editar</a>
                            <form action="{{ route('users.destroy', $user->id) }}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm float-left">Excluir</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12">
            @include('partials.pagination', ['paginator' => $users])
        </div>
    </div>
@endsection

@section('page_level_js')
    <script>

        /** Ajax de criação de usuario
         */
        $('#formNewUser').submit(function(e){
            e.preventDefault();
            let formData = new FormData();
            let name = $('#name').val();
            let email = $('#email').val();
            let category_id = $('#category_id').val();
            let token = $('input[name="_token"]').val();

            formData.append('name', name);
            formData.append('email', email);
            formData.append('category_id', category_id);
            formData.append('token', token);

            let url = '{{ route('ajax.users.store', $user->id) }}';

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    name: name,
                    email: email,
                    category_id: category_id,
                    _token: '{{csrf_token()}}'
                },
                success: function (user) {

                    $('.table>tbody').prepend("<tr>" +
                        "<td>" +user.name+
                        "</td>" +

                        "<td>" +user.email+
                        "</td>" +

                        "<td>" +user.categoryName+
                        "</td>" +

                        "<td>" +
                        "<a href='/users/"+user.id+"/edit' class='btn btn-primary btn-sm float-left mr-1'>Editar</a>" +
                        "<form action='/users/destroy/"+user.id+"' method='post'>" +
                            "<input type='hidden' name='_token' value='{{csrf_token()}}'>"+
                            "<button type='submit' class='btn btn-danger btn-sm float-left'>Excluir</button>" +
                        "</form>" +
                        "</td>" +
                        "</tr>");

                    $('#novoUsuario').modal('hide');
                    $('#name').val('');
                    $('#email').val('');
                    $('#category_id').val('');

                },
                error: function (data, textStatus, errorThrown) {
                    alert('Não foi possível cadastrar o usuario.');
                },
            });

        });

    </script>
@endsection
