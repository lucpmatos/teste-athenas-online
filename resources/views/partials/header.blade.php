<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ (request()->routeIs('users*')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('users.index') }}">Usuários</a>
            </li>
            <li class="nav-item {{ (request()->routeIs('categories*')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('categories.index') }}">Categorias</a>
            </li>
        </ul>
    </div>
</nav>

<div class="jumbotron">
    <h1 class="display-4">@yield('title')</h1>
</div>
