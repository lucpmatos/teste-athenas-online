@extends('template')

@section('title')
    Categorias
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{ route('categories.index') }}">Voltar</a>
        </div>
        <div class="col-12">
            <hr/>
        </div>
    </div>
    <form action="{{ route('categories.store') }}" method="post" class="row">
        @csrf

        <div class="col-12">
            <div class="form-group">
                <label for="name">Nome:</label>
                <input type="text" name="name" id="name" placeholder="Nome" class="form-control" required>
            </div>
        </div>

        <div class="col-12">
            <button type="submit" class="btn btn-success float-right">Salvar</button>
        </div>

    </form>
@endsection
