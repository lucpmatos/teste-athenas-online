@extends('template')

@section('title')
    Categorias
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            {{--<a href="{{ route('categories.create') }}" class="btn btn-primary">Novo</a>--}}
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#novaCategoria">
                Novo
            </button>
            <!-- Modal -->
            <div class="modal fade" id="novaCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <form id="formNewCategory" action="" class="modal-content">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nova categoria</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Nome:</label>
                                        <input type="text" name="name" id="name" placeholder="Nome" class="form-control" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <hr/>
        </div>
        <div class="col-12">
            @include('partials.message')
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary btn-sm float-left mr-1">Editar</a>
                            <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm float-left">Excluir</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12">
            @include('partials.pagination', ['paginator' => $categories])
        </div>
    </div>
@endsection

@section('page_level_js')
    <script>

        /** Ajax de criação de usuario
         */
        $('#formNewCategory').submit(function(e){
            e.preventDefault();
            let formData = new FormData();
            let name = $('#name').val();
            let token = $('input[name="_token"]').val();

            formData.append('name', name);
            formData.append('token', token);

            let url = '{{ route('ajax.categories.store', $category->id) }}';

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    name: name,
                    _token: '{{csrf_token()}}'
                },
                success: function (category) {

                    $('.table>tbody').prepend("<tr>" +
                        "<td>" +category.name+
                        "</td>" +

                        "<td>" +
                        "<a href='/categories/"+category.id+"/edit' class='btn btn-primary btn-sm float-left mr-1'>Editar</a>" +
                        "<form action='/categories/destroy/"+category.id+"' method='post'>" +
                        "<input type='hidden' name='_token' value='{{csrf_token()}}'>"+
                        "<button type='submit' class='btn btn-danger btn-sm float-left'>Excluir</button>" +
                        "</form>" +
                        "</td>" +
                        "</tr>");

                    $('#novaCategoria').modal('hide');
                    $('#name').val('');

                },
                error: function (data, textStatus, errorThrown) {
                    alert('Não foi possível cadastrar a categoria.');
                },
            });

        });

    </script>
@endsection
