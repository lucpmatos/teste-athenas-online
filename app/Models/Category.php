<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
    	'name'
	];

	/** Categoria tem muitos usuarios
	 * @return HasMany
	 */
	public function users(): HasMany
	{
		return $this->hasMany(User::class);
	}
}
