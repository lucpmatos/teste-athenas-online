<?php


namespace App\Business\Services\Categories;


use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Throwable;

class CategoriesServices
{

	/** Criador de categorias
	 * @param array $data
	 * @return Category
	 */
	public function creator(array $data): Category
	{
		/** Cadastrando nova categoria
		 */
		try {
			DB::beginTransaction();
			$category = Category::create($data);
			DB::commit();
		}catch (Throwable $e){
			report($e);
		}
		return $category;
	}

	/** Atualizador de categorias
	 * @param array $data
	 * @param Category $category
	 * @return Category
	 */
	public function updater(array $data, Category $category): Category
	{
		try {
			DB::beginTransaction();
			$category->update($data);
			DB::commit();
		}catch (Throwable $e){
			report($e);
		}
		return $category;
	}

	/** Excluidor de categorias
	 * @param Category $category
	 * @return bool
	 */
	public function destroyer(Category $category): bool
	{
		/** Guardando o ID para verificar exclusão
		 */
		$id = $category->id;

		/** Excluindo a categoria
		 */
		$category->delete();

		/** Verificando a exclusão e retornando
		 */
		$check = Category::find($id);
		if(!$check){
			return true;
		}else{
			return false;
		}
	}

}
