<?php


namespace App\Business\Services\Users;


use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class UsersServices
{

	/** Criador de usuarios
	 * @param array $data
	 * @return User
	 */
	public function creator(array $data): User
	{
		/** Gerando a senha
		 */
		$data['password'] = Hash::make('12345678');

		/** Cadastradno o novo usuario
		 */
		try {
			DB::beginTransaction();
			$user = User::create($data);
			DB::commit();
		}catch (Throwable $e){
			report($e);
		}

		return $user;
	}

	/** Atualizador de usuarios
	 * @param array $data
	 * @param User $user
	 * @return User
	 */
	public function updater(array $data, User $user): User
	{
		try {
			DB::beginTransaction();
			$user->update($data);
			DB::commit();
		}catch (Throwable $e){
			report($e);
		}
		return $user;
	}

	/** Excluidor de usuarios
	 * @param User $user
	 * @return bool
	 */
	public function destroyer(User $user): bool
	{
		/** Guardando o ID para verificar exclusão
		 */
		$id = $user->id;

		/** Excluindo o usuario
		 */
		$user->delete();

		/** Verificando a exclusão e retornando
		 */
		$check = User::find($id);
		if(!$check){
			return true;
		}else{
			return false;
		}
	}

}
