<?php

namespace App\Http\Controllers;

use App\Business\Services\Users\UsersServices;
use App\Models\Category;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{

	/** Index de usuarios
	 * @return Application|Factory|View
	 */
	public function index(): View|Factory|Application
	{
		$users = User::latest()->paginate(10);
		$categories = Category::all();
		return view('users.index',compact('users','categories'));
	}

	/** Create de usuarios
	 * @return Application|Factory|View
	 */
	public function create(): View|Factory|Application
	{
		$categories = Category::all();
		return view('users.create',compact('categories'));
	}

	/** Store de usuarios
	 * @param Request $request
	 * @param UsersServices $usersServices
	 * @return RedirectResponse
	 */
	public function store(Request $request, UsersServices $usersServices): RedirectResponse
	{
		/** Cadastrando usuario
		 */
		$user = $usersServices->creator(
			data: $request->except('_token')
		);

		/** Tratando retorno
		 */
		if($user instanceof \app\Models\User){
			return redirect()->route('users.index')
				->with('message', "Usuário {$user->name} cadastrado com sucesso.");
		}else{
			return redirect()->route('users.index')
				->with('message', "Não foi possível cadastrar o usuário.");
		}
	}

	/** Edit de usuarios
	 * @param User $user
	 * @return Application|Factory|View
	 */
	public function edit(User $user)
	{
		$categories = Category::all();
		return view('users.edit',compact('user','categories'));
	}

	/** Update de usuarios
	 * @param User $user
	 * @param Request $request
	 * @param UsersServices $usersServices
	 * @return RedirectResponse
	 */
	public function update(User $user, Request $request, UsersServices $usersServices): RedirectResponse
	{
		/** Atualizando usuario
		 */
		$newUser = $usersServices->updater(
			data: $request->except('_token'),
			user: $user
		);

		/** Tratando retorno
		 */
		if($newUser instanceof \app\Models\User){
			return redirect()->route('users.index')
				->with('message', "Usuário {$newUser->name} atualizado com sucesso.");
		}else{
			return redirect()->route('users.index')
				->with('message', "Não foi possível atualizar o usuário.");
		}

	}

	/** Destroy de usuarios
	 * @param User $user
	 * @param UsersServices $usersServices
	 * @return RedirectResponse
	 */
	public function destroy(User $user, UsersServices $usersServices): RedirectResponse
	{
		/** Excluindo usuario
		 */
		$deleted = $usersServices->destroyer(
			user: $user
		);

		/** Tratando o retorno e redirecionando
		 */
		if($deleted == true){
			return redirect()->route('users.index')
				->with('message', "Usuário exluído com sucesso.");
		}else{
			return redirect()->route('users.index')
				->with('message', "Não foi possível excluir o usuário.");
		}
	}

}
