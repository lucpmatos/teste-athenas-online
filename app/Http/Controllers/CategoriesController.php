<?php

namespace App\Http\Controllers;

use App\Business\Services\Categories\CategoriesServices;
use App\Models\Category;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

	/** Index de categorias
	 * @return Application|Factory|View
	 */
	public function index(): View|Factory|Application
	{
		$categories = Category::latest()->paginate(10);
		return view('categories.index',compact('categories'));
	}

	/** Create de categorias
	 * @return Application|Factory|View
	 */
	public function create(): View|Factory|Application
	{
		return view('categories.create');
	}

	/** Store de categorias
	 * @param Request $request
	 * @param CategoriesServices $categoriesServices
	 * @return RedirectResponse
	 */
	public function store(Request $request, CategoriesServices $categoriesServices): RedirectResponse
	{
		/** Criando a categoria
		 */
		$category = $categoriesServices->creator(
			data: $request->except('_token')
		);

		/** Tratando retorno
		 */
		if($category instanceof \app\Models\Category){
			return redirect()->route('categories.index')
				->with('message', "Categoria {$category->name} cadastrada com sucesso.");
		}else{
			return redirect()->route('categories.index')
				->with('message', "Não foi possível cadastrar a categoria.");
		}
	}

	/** Edit de categorias
	 * @param Category $category
	 * @return Application|Factory|View
	 */
	public function edit(Category $category): View|Factory|Application
	{
		return view('categories.edit',compact('category'));
	}

	/** Update de categorias
	 * @param Category $category
	 * @param Request $request
	 * @param CategoriesServices $categoriesServices
	 * @return RedirectResponse
	 */
	public function update(Category $category, Request $request, CategoriesServices $categoriesServices): RedirectResponse
	{
		/** Atualizando categoria
		 */
		$newCategory = $categoriesServices->updater(
			data: $request->except('_token'),
			category: $category
		);

		/** Tratando retorno
		 */
		if($newCategory instanceof \app\Models\Category){
			return redirect()->route('categories.index')
				->with('message', "Categoria {$newCategory->name} atualizada com sucesso.");
		}else{
			return redirect()->route('categories.index')
				->with('message', "Não foi possível atualizar a categoria.");
		}
	}

	/** Destroy de categorias
	 * @param Category $category
	 * @param CategoriesServices $categoriesServices
	 * @return RedirectResponse
	 */
	public function destroy(Category $category, CategoriesServices $categoriesServices): RedirectResponse
	{
		/** Verificando se existem usuários atribuídos a essa categoria
		 */
		$checkUsers = User::query()
			->select()
			->where('category_id', $category->id)
			->get();
		if($checkUsers->count() > 0){
			return redirect()->route('categories.index')
				->with('message', "Não foi possível excluir a categoria. Existem {$checkUsers->count()} usuário(s) atribuídos.");
		}

		/** Excluindo usuario
		 */
		$deleted = $categoriesServices->destroyer(
			category: $category
		);

		/** Tratando o retorno e redirecionando
		 */
		if($deleted == true){
			return redirect()->route('categories.index')
				->with('message', "Categoria exluída com sucesso.");
		}else{
			return redirect()->route('categories.index')
				->with('message', "Não foi possível excluir a categoria.");
		}
	}

}
