<?php

namespace App\Http\Controllers;

use App\Business\Services\Categories\CategoriesServices;
use App\Business\Services\Users\UsersServices;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class AjaxRequests extends Controller
{

	/** Criar usuarios
	 * @param Request $request
	 * @param UsersServices $usersServices
	 * @return User|false
	 */
	public function createUser(Request $request, UsersServices $usersServices): bool|User
	{
		/** Cadastrando usuario
		 */
		$user = $usersServices->creator(
			data: $request->except('_token')
		);

		$user['categoryName'] = $user->category->name;

		if($user instanceof User){
			return $user;
		}else{
			return false;
		}
	}

	/** Criar categorias
	 * @param Request $request
	 * @param CategoriesServices $categoriesServices
	 * @return Category|false
	 */
	public function createCategory(Request $request, CategoriesServices $categoriesServices): bool|Category
	{
		/** Criando a categoria
		 */
		$category = $categoriesServices->creator(
			data: $request->except('_token')
		);

		/** Tratando retorno
		 */
		if($category instanceof Category){
			return $category;
		}else{
			return false;
		}
	}

}
