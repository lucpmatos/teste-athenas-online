## Teste Athenas Online

Este teste consiste na resolução do problema apresentado no desafio para a vaga Desenvolvedor PHP Pleno - Remoto - Athenas Tecnologia:

## Configuração de Servidor

Configurações de ambiente onde essa aplicação foi desenvolvida:

- Sistema: WSL (Ubuntu 20.04 LTS)
- Server: Nginx 1.18.0
- Database: Mysql 8.0.22
- PHP: 8.0.1

/var/nginx/sites-available/testeathenas.local
```
server{
    listen 80;
    
    root /var/www/teste-athenas-online/public;
    
    add__header X-Frame-Options "SAMEORIGIN";
    add__header X-XSS-Protection "1; mode=block";
    add__header X_Content-Type-Options "nosniff";
    
    index index.php index.html index.htm index.nginx-debian.html;
    
    charset utf-8;
    
    server_name testeathenas.local;
    
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    location ~ \.php${
        fastcgi_pass unix:/var/run/php/php8.0-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
    
    location ~ /\.ht {
        deny all;
    }
    
    location ~ /\.(?!well-know).* {
        allow all;
    }
}
```

Obs: Algumas configurações podem variar dependendo de cada ambiente.

## Laravel

Após criar os links simbólicos do Nginx e configurar o arquivo .env na raiz do projeto, basta rodar os comandos a seguir dentro do diretório raiz do projeto:

- `php artisan key:generate`
- `php artisan migrate:fresh --seed`

## Sobre o desenvolvedor
### Lucas Pedro

Sou um desenvolvedor fascinado pela tecnologia web no back-end, e
em complemento o front-end. Acreditando na integração entre esses
dois mundos, dedico meus esforços para transformar a navegação na
web em um lugar mais confiável e intuitivo

- [LinkedIn](https://www.linkedin.com/in/lucas-pedro-84b574a0/)
- [Facebook](https://www.facebook.com/lucas.pedro.188/)

## Projeto particular
### Lecionar

O sistema Lecionar foi desenvolvido pela <a href="http://codebased.com.br/" target="_blank">Codebased</a> com o objetivo de trazer mais integração entre a escola, aluno e responsáveis, criando um ambiente mais colaborativo e entrosado auxiliando na gestão da escola.

[http://lecionar.app/](http://lecionar.app/)

## Conclusão

Esse teste foi desenvolvido com amor e paixão pelo código. Espero atender às espectativas.

att

Lucas Pedro

(21)96512-7584
