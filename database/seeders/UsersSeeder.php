<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		User::create([
			'name' => 'Jorge da Silva',
			'email' => 'jorge@terra.com.br',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Flavia Monteiro',
			'email' => 'flavia@globo.com',
			'password' => Hash::make('12345678'),
			'category_id' => 2
		]);

		User::create([
			'name' => 'Marcos Frota Ribeiro',
			'email' => 'ribeiro@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 2
		]);

		User::create([
			'name' => 'Raphael Souza Santos',
			'email' => 'rsantos@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Pedro Paulo Mota',
			'email' => 'ppmota@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Winder Carvalho da Silva',
			'email' => 'winder@hotmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 3
		]);

		User::create([
			'name' => 'Maria da Penha Albuquerque',
			'email' => 'mpa@hotmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 3
		]);

		User::create([
			'name' => 'Rafael Garcia Souza',
			'email' => 'rgsouza@hotmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 3
		]);

		User::create([
			'name' => 'Tabata Costa',
			'email' => 'tabata_costa@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 2
		]);

		User::create([
			'name' => 'Ronil Camarote',
			'email' => 'camarote@terra.com.br',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Joaquim Barbosa',
			'email' => 'barbosa@globo.com',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Eveline Maria Alcantra',
			'email' => 'ev_alcantra@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 2
		]);

		User::create([
			'name' => 'João Paulo Vieira',
			'email' => 'jpvieria@gmail.com',
			'password' => Hash::make('12345678'),
			'category_id' => 1
		]);

		User::create([
			'name' => 'Carla Zamborlini',
			'email' => 'zamborlini@terra.com.br',
			'password' => Hash::make('12345678'),
			'category_id' => 3
		]);


    }
}
