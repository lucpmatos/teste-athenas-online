<?php

use App\Http\Controllers\CategoriesController;
use Illuminate\Support\Facades\Route;

/** Rotas de categorias
 */
Route::resource('categories', CategoriesController::class)
	->except('update','destroy');

Route::group(['as'=>'categories.'], function(){

	Route::post('categories/update/{category}', [CategoriesController::class, 'update'])
		->name('update');

	Route::post('categories/destroy/{category}', [CategoriesController::class, 'destroy'])
		->name('destroy');

});
