<?php

use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;


/** Rotas de usuarios (pessoas)
 */
Route::resource('users', UsersController::class)
	->except(['update', 'destroy']);

Route::group(['as'=>'users.'], function(){

	Route::post('users/update/{user}', [UsersController::class, 'update'])
		->name('update');

	Route::post('users/destroy/{user}', [UsersController::class, 'destroy'])
		->name('destroy');

});

