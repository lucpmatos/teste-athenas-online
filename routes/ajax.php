<?php

use App\Http\Controllers\AjaxRequests;
use Illuminate\Support\Facades\Route;

/** Rotas de requisições ajax
 */
Route::group(['as'=>'ajax.'], function(){

	Route::group(['as'=>'users.'], function(){

		Route::post('/ajax/users/{user}', [AjaxRequests::class, 'createUser'])
			->name('store');

	});

	Route::group(['as'=>'categories.'], function(){

		Route::post('/ajax/categories/{category}', [AjaxRequests::class, 'createCategory'])
			->name('store');

	});

});
